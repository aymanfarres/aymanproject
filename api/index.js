const express= require('express')
const cors= require('cors')
const mongoose = require('mongoose')
const User = require('./models/User.js');
const bcrypt = require('bcryptjs');
const jwt= require('jsonwebtoken')

require('dotenv').config()


const app= express()
app.use(express.json())

const salt= bcrypt.genSaltSync(12)
const jwtsalt= 'jhnkdfhhduhdkbf'
app.use(cors({
 credentials: true,
 origin:'http://localhost:5173'

}))
const connect= ()=>{
     mongoose.connect(process.env.MONGO)
     return console.log('connected')

}

app.post('/register',async (req,res)=>{
    const {name, email, password}= req.body
   try{const userDoc= await User.create({
        name,
        email,
        password: bcrypt.hashSync(password, salt)
    })
    res.json(userDoc)

}catch(err){
        res.status(422).json(err)
    }
})

app.post('/login',async(req, res)=>{
    const{email, password}= req.body
    const userDoc=await User.findOne({email})
     if(userDoc){
        const passOk = bcrypt.compareSync(password, userDoc.password);
        if (passOk){
           jwt.sign({email:userDoc.email, id:userDoc._id},jwtsalt, {},(err, token)=>{
       if(err) throw err;
       res.cookie('token', token).json('pass ok ')
           })
        }
    }
    else{
        res.json('no found')
    }
})


app.listen(4000,()=>{
    console.log('lets go');
    connect()
})